import numpy as np
import matplotlib.pyplot as plt


#MANCHESTER encode        
def encode_manchester(array):
    manchester = []
    for x in array:
        if x:
            manchester.append(0)
            manchester.append(1)
        else:
            manchester.append(1)
            manchester.append(0)
    return manchester

#funcion que grafica
def plot_binary(model, array, encoded):
    if model == 'manchester' or model == 'differential-manchester':
        x = np.arange(0,len(array)+0.5, 0.5)
        x2 = np.arange(0,len(array)+1)
    else:
        x = np.arange(0,len(encoded)+1)
        x2 = x
    
    #PLOTS
    plt.subplot(211)
    plt.xlim(0, len(array))
    plt.ylim(-0.5, 1.5)
    
    plt.ylabel('Value')
    plt.title('Original')
    
    array = [int(z) for z in array]
    for i in range(len(array)):
        plt.text(i+0.4, 1.2, array[i])
    
    plt.grid()
    plt.xticks(x2)    
    plt.step(x2, [array[0]]+array)


    #We need add the first number twice to see properly on plot
    plt.subplot(212)
    plt.xlim(0, len(array))
    plt.ylim(-0.5, 1.5)
    
    plt.ylabel('Value')
    plt.title(model)
    
    for i in range(len(array)):
            plt.text(i+0.4, 1.2, array[i])    
    
    plt.grid()
    plt.xticks(x2)        
    plt.step(x, [encoded[0]]+encoded)
    plt.show()    



#Creando un diccionario:

args = dict()    #Crear un diccionario.
rand = np.random.randint(2, size=(10,))  #genera aleatoriamente 1 o 0 en un array.
args['sequence'] = [bool(x) for x in rand] # guardo en el diccionario, el array pero parseado a true o false
args['model'] = "manchester" # modelo.
encoded = [0, 0];

#DATOS PARA GRAFICAR
array = args['sequence']
encoded = encode_manchester(array)
model = args['model']

#FUNCION GRAFICADORA
plot_binary(model, array, encoded)

print(args)